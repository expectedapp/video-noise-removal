# Video Noise Removal

This is a free and open source video noise removal using ffmpeg and sox

There is a variable inside the script called **Hz** you need to change the frequencies inside it, then script will remove those frequencies from video. this script also do an automatically noise removal in addition to removing noisy frequencies.
for see what Hz you have to remove from your video I suggest you
to **see this video:** https://www.youtube.com/watch?v=qPBvQ4yupX8&t=3s

for example:
```shell
startHz=200
step=100
endHz=1000
Hz=($(seq $startHz $step $endHz))
```

this means that it will remove 200Hz 300Hz 400Hz ... 1000Hz from the audio section of the given video file.

**Usage:**
NoiseRemoval.sh "Your Vido File.mkv or whatever extension"